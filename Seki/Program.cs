﻿
using Antlr4.Runtime;
using System;
using System.IO;

namespace Seki
{
    public class Program
    {
        static void Main(string[] args)
        {
            var filepath = @"D:\Personal\Semester 1\Programming Language design\test.seki.txt";
            var input = File.ReadAllText(filepath);
            var str = new AntlrInputStream(input);
            var lexer = new arithmeticLexer(str);
            var tokens = new CommonTokenStream(lexer);

            PrintTokensIdentified(lexer, tokens);

            var parser = new arithmeticParser(tokens);
            ParserRuleContext context = parser.Context;
            String treeString = context.ToStringTree(parser);

            PrintPrettyLispTree(treeString);

            var listener = new ErrorListener<IToken>();
            parser.AddErrorListener(listener);
            var tree = parser.file();
            if (listener.had_error)
            {
                Console.WriteLine("error in parse.");
            }
            else
            {
                Console.WriteLine("parse completed.");
                Console.WriteLine(tree.OutputTree(tokens));
            }

            Console.Read();
        }

        private static void PrintPrettyLispTree(string tree)
        {
            int indentation = 1;
            foreach (var c in tree.ToCharArray())
            {
                if (c == '(')
                {
                    if (indentation > 1)
                    {
                        Console.WriteLine();
                    }
                    for (int i = 0; i < indentation; i++)
                    {
                        Console.Write("  ");
                    }
                    indentation++;
                }
                else if (c == ')')
                {
                    indentation--;
                }
                Console.Write(c);
            }
            Console.WriteLine();
        }

        private static void PrintTokensIdentified(arithmeticLexer lexer, CommonTokenStream tokens)
        {
            tokens.Fill();
            Console.WriteLine("\n[TOKENS]");

            foreach (var t in tokens.GetTokens())
            {
                String symbolicName = lexer.Vocabulary.GetSymbolicName(t.Type);
                String literalName = lexer.Vocabulary.GetLiteralName(t.Type);
                Console.WriteLine($" Name: {(symbolicName ?? literalName)}, " +
                         $" Value: { t.Text.Replace("\r", "\\r").Replace("\n", "\\n").Replace("\t", "\\t")}");
            }
        }
    }
}
