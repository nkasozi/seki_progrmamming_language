﻿using Antlr4.Runtime;
using System;
using System.IO;

namespace Johatsu
{
    public class Program
    {
        static arithmeticParser parser;
        static void Main(string[] args)
        {
            var filepath = @"D:\Personal\Semester 1\Programming Language design\test.seki.txt";
            var input = File.ReadAllText(filepath);
            var str = new AntlrInputStream(input);
            var lexer = new arithmeticLexer(str);
            var tokens =  new CommonTokenStream(lexer);

            //var test =  new int[10];

            PrintTokensIdentified(lexer, tokens);

            parser = new arithmeticParser(tokens);
            var listener = new ErrorListener<IToken>();

            parser.AddErrorListener(listener);

            var tree = parser.file();

            if (listener.had_error)
            {
                Console.WriteLine("error in parse.");
            }
            else
            {

                Console.WriteLine("parse completed.");
                //ExploreAST(parser.file());
                Console.WriteLine(tree.OutputTree(tokens));
            }

            Console.Read();
        }

        private static void PrintPrettyLispTree(string tree)
        {
            int indentation = 1;
            foreach (var c in tree.ToCharArray())
            {
                if (c == '(')
                {
                    if (indentation > 1)
                    {
                        Console.WriteLine();
                    }
                    for (int i = 0; i < indentation; i++)
                    {
                        Console.Write("  ");
                    }
                    indentation++;
                }
                else if (c == ')')
                {
                    indentation--;
                }
                Console.Write(c);
            }
            Console.WriteLine();
        }

        private static void PrintTokensIdentified(arithmeticLexer lexer, CommonTokenStream tokens)
        {
            tokens.Fill();
            Console.WriteLine("\n[TOKENS]");

            foreach (var t in tokens.GetTokens())
            {
                String symbolicName = lexer.Vocabulary.GetSymbolicName(t.Type);
                String literalName = lexer.Vocabulary.GetLiteralName(t.Type);
                Console.WriteLine($" Name: {(symbolicName ?? literalName)}, " +
                         $" Value: { t.Text.Replace("\r", "\\r").Replace("\n", "\\n").Replace("\t", "\\t")}");
            }
        }

        static void ExploreAST(ParserRuleContext ctx, int indentLevel = 0)
        {
            var ruleName = parser.RuleNames[ctx.RuleIndex];
            var sep = "".PadLeft(indentLevel);
            bool keepRule = ctx.ChildCount > 1;
            if (keepRule)
              Console.WriteLine(sep + ruleName);
            foreach (var c in ctx?.children)
            {
                if (c is ParserRuleContext)
                    ExploreAST((ParserRuleContext)c, indentLevel + ((keepRule) ? 4 : 0));
                else
                {
                    var sep2 =
                        "".PadLeft(indentLevel + ((keepRule) ? 4 : 0));
                    Console.WriteLine(sep2 + c.ToString());
                }
            }
        }
    }
}
