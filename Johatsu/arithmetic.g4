﻿grammar arithmetic;

file:   (method_declaration | (variable_declaration TERMINATOR))+ ;

method_declaration:  return_type  method_name LPAREN parameter_declarations? RPAREN block_of_statements;

method_name: VARIABLE_NAME;

variable_declaration: type_declaration | data_type_declaration | array_type_declaration;

type_declaration: 'type' data_type VARIABLE_NAME (',' VARIABLE_NAME)* ('=' expression)?;

data_type_declaration: data_type VARIABLE_NAME (',' VARIABLE_NAME)* ('=' expression)?;

array_type_declaration: data_type '[' expression ']'  ('[' expression ']')* VARIABLE_NAME ('=' expression)?;

return_type:   data_type | 'void' ; 

parameter_declarations:   parameter_declaration (',' parameter_declaration)*;

parameter_declaration:  data_type  VARIABLE_NAME;

block_of_statements:  LCURLY statement* RCURLY ;   

data_type: primitive_data_type | user_defined_data_type;

primitive_data_type:   'float' | 'int' | 'string' | 'double' | 'bool'|'char' ; 

type_def_declaration: basic_type_def_declaration | struct_type_def_declaration;

basic_type_def_declaration: 'typedef'  primitive_data_type VARIABLE_NAME;

struct_type_def_declaration: 'typedef' 'struct' LCURLY (variable_declaration TERMINATOR)* RCURLY VARIABLE_NAME;

statement:   block_of_statements
    |   variable_declaration TERMINATOR
	|   type_def_declaration TERMINATOR
	|	variable_assignment TERMINATOR
    |   'if' LPAREN expression RPAREN  statement ('elseif' LPAREN expression RPAREN statement)? ('else' statement)?
    |	'while' LPAREN expression RPAREN  block_of_statements 
	|   read_method TERMINATOR
    |   write_method TERMINATOR
    |   'return' expression? TERMINATOR 
    |   expression '=' expression TERMINATOR
    |   expression TERMINATOR          
    ;

method_call: method_name '(' (data_type_cast_to_directive? VARIABLE_NAME)? (',' data_type_cast_to_directive? VARIABLE_NAME)* ')';

variable_assignment: basic_variable_assignment | array_variable_assignment  ;

basic_variable_assignment:  VARIABLE_NAME '=' data_type_cast_to_directive? expression;

data_type_cast_to_directive: ( '(' data_type ')' );

array_variable_assignment: array_variable '='  expression;

array_variable : VARIABLE_NAME '[' expression ']'  ('[' expression ']')* ;

read_method : 'read' expression (',' expression)*; 

write_method : 'write' expression (',' expression)*; 

expression:   VARIABLE_NAME LPAREN parameter_list? RPAREN  
    |   VARIABLE_NAME LSQUARE expression RSQUARE       
    |   ('-' | '!') expression
    |   expression ('/' | '%' | '*') expression
    |   expression ('+'|'-') expression
    |   expression '==' expression
    |   expression '!=' expression
    |   expression ('>=' | '>') expression
    |   expression ('<=' | '<') expression
    |   expression ('&&' |'||')  expression
	|   method_call
	|   array_variable
    |   VARIABLE_NAME 
    |   INTEGER
    |	STRING
    |	DOUBLE
	|   CHARACTER
    |   LPAREN expression RPAREN
    ;

parameter_list : expression (',' expression)* ; 

user_defined_data_type: VARIABLE_NAME;

VARIABLE_NAME  :   LETTER (LETTER | DIGIT)* ;

fragment LETTER : [a-zA-Z] | UNDERSCORE ;

INTEGER :   [0-9]+ ;

LPAREN :   '(' ;

UNDERSCORE :   '_' ;

RPAREN :   ')' ;

RCURLY :   '}' ;

LCURLY :   '{' ;

RSQUARE :   ']' ;

LSQUARE :   '[' ;

TERMINATOR :   ';' ;

STRING: '"' (ESC|.)*? '"' ;

CHARACTER: '\'' (ESC|.)*? '\'' ;

fragment ESC : '\\"' | '\\\\' ;

DOUBLE: DIGIT+ '.' DIGIT* | '.' DIGIT+ ;

fragment DIGIT : [0-9] ; 

WS  :   [ \t\n\r]+ -> skip ;

COMMENTS :   '//' .*? '\n' -> skip;

MULTI_LINE_COMMENTS :   '/*' .*? '*/' -> skip;